import subprocess
import sys
import os
import json
import csv
import requests
import pandas as pd
import matplotlib.pyplot as plt
import re
from sklearn.feature_extraction.text import CountVectorizer

# This function retrieves the entire audience, the max entries per request is 100 so it is hardcoded to save time
# seg is the name of the audience to scan, fields is a list of field names, token is the Lytics API token
# example input parameters:
#seg = 'AgTech_House_NL_Test'
#fields = ['_uid','frid']

def scanSegmentToCSV(seg, fields, token):
    limit = 100
    print(seg.lower())
    reqString = 'https://api.lytics.io/api/segment/'+str(seg.lower())+'/scan'
    headers = {'Content-Type': 'application/json','Authorization': token}
    params = {'limit':limit,'fields':fields}
    with open(str(seg)+'.csv', 'w') as outf:
        dw = csv.DictWriter(outf, fields)
        dw.writeheader()
        next = 'Start'
        step = 0
        while next!='':
            response = requests.get(reqString, headers=headers, params=params)
            fullReturned = json.loads(response.content.decode('utf-8'))
            if fullReturned['message']!='success':
                print(fullReturned['message'])
                break
            else:
                if step==0:
                    print(fullReturned['message'])
                    print(fullReturned['total'])
                next = fullReturned['_next']
                if next != '':
                    print('Scanned in '+str(step)+' - '+str(step+limit))
                    step+=limit
                params = {'fields':fields, 'start':next}
            fullReturnedData = fullReturned['data']
            csvData = [{d:fullReturnedData[i][d] for d in fields if d in fullReturnedData[i].keys()} for i in range(len(fullReturnedData))]
            for row in csvData:
                dw.writerow(row)

# This function retrieves the entire audience, like scanSegmentToCSV,
# while also vectorizing the article words list for each user into the csv and printing the words list for the entire group
# seg = name of the audience to scan, string
# fields = list of field names, strings
# token = Lytics API token, string
# stop = maximum users to scan, int

# slow alternative to exporting from lytics to GBQ
def scanSegmentToWordVectorCSV(seg, fields, token, stop):
    limit = 100
    if 'full_urls' not in fields:
        fields.append('full_urls')
    print(seg.lower())
    reqString = 'https://api.lytics.io/api/segment/'+str(seg.lower())+'/scan'
    headers = {'Content-Type': 'application/json','Authorization': token}
    params = {'limit':limit,'fields':fields}
    success = 0
    with open(str(seg)+'_vector.csv', 'w') as outf:
        columns = fields.copy()
        columns.remove('full_urls')
        columns.append('words')
        dw = csv.DictWriter(outf, columns)
        dw.writeheader()
        next = 'Start'
        step = 0
        allURL = []
        while next!='':
            response = requests.get(reqString, headers=headers, params=params)
            fullReturned = json.loads(response.content.decode('utf-8'))
            if fullReturned['message']!='success' or step >= stop:
                break
            else:
                if step==0:
                    success = (fullReturned['message']=='success')
                    print(fullReturned['message'])
                    print(fullReturned['total'])
                next = fullReturned['_next']
                if next != '':
                    print('Scanned in '+str(step)+' - '+str(step+limit))
                    step+=limit
                params = {'fields':fields, 'start':next}
            fullReturnedData = fullReturned['data']
            csvData = [{d:fullReturnedData[i][d] for d in fields if d in fullReturnedData[i].keys()} for i in range(len(fullReturnedData))]
            for i in range(len(csvData)):
                wordEntry = {'words':''}
                if 'full_urls' in csvData[i]:
                    fullUrls = csvData[i]['full_urls']
                    allURL = allURL+fullUrls
                    wordEntry['words'] = urlToContentWords(fullUrls,0,0,0)[0]
                    del csvData[i]['full_urls']
                csvData[i].update(wordEntry)
                dw.writerow(csvData[i])
    if success:
        # save all file formats for entire segment
        _ = urlToContentWords(allURL,1,1,1,seg=seg)

# This function retrieves the entire audience, like scanSegmentToCSV,
# while also presenting selected keywords in full urls into a pivoted table to include in the output csv
# seg = name of the audience to scan, string
# fields = list of field names, strings
# kwl = list of keywords to turn into a 'viewed' category in the output, strings
# token = Lytics API token

def scanSegmentToKeywordCSV(seg, fields, kwl, token, stop, startKey = None):
    limit = 100
    singleWords = [stemWordP2(term.strip().lower()) for term in kwl if '-' not in term]
    groupWords = ['-'.join([stemWordP2(word.strip().lower()) for word in term.split('-')]) for term in [term for term in kwl if '-' in term]]
    repWords = [term.replace('-','_') for term in groupWords]
    kwl = list(dict.fromkeys(singleWords + repWords))
    print(kwl)
    if 'full_urls' not in fields:
        fields.append('full_urls')
    print(seg.lower())
    reqString = 'https://api.lytics.io/api/segment/'+str(seg.lower())+'/scan'
    headers = {'Content-Type': 'application/json','Authorization': token}
    params = {'limit':limit,'fields':fields}
    if startKey != None:
        params['start']=startKey
    if not os.path.exists('STORE/LYTICS/'):
        os.makedirs('STORE/LYTICS/')
    with open('STORE/LYTICS/'+str(seg)+'.csv', 'w') as outf:
        fields.append('pageTotal')
        columns = (fields+kwl)
        columns.remove('full_urls')
        dw = csv.DictWriter(outf, columns)
        dw.writeheader()
        next = 'Start'
        step = 0
        while next!='':
            response = requests.get(reqString, headers=headers, params=params)
            fullReturned = json.loads(response.content.decode('utf-8'))
            if fullReturned['message']!='success' or step >= stop:
                print(fullReturned['message'])
                break
            else:
                if step==0:
                    print(fullReturned['message'])
                    print(fullReturned['total'])
                next = fullReturned['_next']
                if next != '':
                    print('Scanned in '+str(step)+' - '+str(step+limit))
                    step+=limit
                    outFile = open(seg+str('_NEXTKEY.txt'),'w')
                    outFile.write(next)
                    outFile.close()
                params = {'fields':fields, 'start':next}
            fullReturnedData = fullReturned['data']
            csvData = [{d:fullReturnedData[i][d] for d in fields if d in fullReturnedData[i].keys()} for i in range(len(fullReturnedData))]
            for i in range(len(csvData)):
                kwDict = {kw: 0 for kw in kwl}
                if 'full_urls' in csvData[i]:
                    contentWords = urlToContentWords(csvData[i]['full_urls'],0,0,0,groupWords=groupWords)
                    allkwDict = contentWords[0]
                    kwDict['pageTotal'] = contentWords[1]
                    for kw in [kw for kw in kwDict.keys() if kw in allkwDict.keys()]:
                        kwDict[kw] = allkwDict[kw]
                    del csvData[i]['full_urls']
                csvData[i].update(kwDict)
                dw.writerow(csvData[i])

# This function transforms a list of full urls into the relevant article string, currently it retreives words if the http directory contains blog or article, or an MP listing starting with tractor, which is standard in AgWeb content
def urlToContentWords(allURL,wordCountCSV, domCountCSV, allWordTxt, groupWords=[], seg='segment'):
    keepArticlePrefix = ['article','blog']
    keepDomPrefix = ['greenbook','machinerypete']
    repWords = [stemWordP2(word.lower().replace('-','_')) for word in groupWords]
    a_str = []
    d_str = []
    for p in allURL:
        dom = re.split('\.com|\.net',p)[0].split('.')[-1]
        d_str.append(dom)
        a_split = re.split('\.com|\.net|\.local',p)[-1]
        if '?mkt_tok' in a_split:
            a_split = a_split.split('?mkt_tok')[0]
        a_split = list(filter(None, a_split.split('/')))
        if len(a_split)>0:
            pagewords = ''
            if any(prefix in a_split for prefix in keepArticlePrefix):
                pagewords = a_split[-1].replace('_','-')
            elif any(d == dom for d in keepDomPrefix):
                a_split = list(filter((lambda x: re.match(r'(?![0-9]{8})', x)), a_split))
                pagewords = '-'.join(a_split).replace('_','-').split('?')[0]
            pagewords = '-'.join([stemWordP2(word.lower()) for word in pagewords.split('-')])
            for group, rep in zip(groupWords, repWords):
                if group in pagewords:
                    pagewords = pagewords.replace(group, rep)
            a_str.append(pagewords)
    a_str = list(dict.fromkeys(a_str))
    dropChar = ['-','\s*']
    comp_regex = re.compile('|'.join(map(re.escape, dropChar)))
    allURLstr = comp_regex.sub(' ',' '.join(a_str))
    allURLstr = re.sub(r'\w*\=\w*|\w*\%\w*|\w*\&\w*','', allURLstr).strip().split(' ')
    words_freq_all = makeWordCountTuples(allURLstr)
    if wordCountCSV:
        outFile = open(seg+str('_wordlist.csv'),'w')
        for k,v in words_freq_all.items():
            outFile.write(str(k)+','+str(v)+'\n')
        outFile.close()
    if domCountCSV:
        dom_freq_all = makeWordCountTuples(d_str)
        outFile = open(seg+str('_domlist.csv'),'w')
        for k,v in dom_freq_all.items():
            outFile.write(str(k)+','+str(v)+'\n')
        outFile.close()
    if allWordTxt:
        outFile = open(seg+str('_allWords.txt'),'w')
        outFile.write(' '.join(allURLstr))
        outFile.close()
    return [words_freq_all,len(a_str)]

# function for making tuple of word counts from list of strings, numbers and stopwords excluded
def makeWordCountTuples(wList):
    try:
        vec = CountVectorizer(stop_words='english').fit(wList)
        bag_of_words = vec.transform(wList)
        sum_words = bag_of_words.sum(axis=0)
        words_freq = [(word, sum_words[0, idx]) for word, idx in vec.vocabulary_.items() if len(word) < 30]
        words_freq.sort(key=lambda tup: tup[1], reverse=True)
        words_freq = {tup[0]:tup[1] for tup in words_freq}
        return words_freq
    except:
        #print('No Word Count: {}'.format(wList))
        return {}

#Certainly the only stemmer for keywords searches, keeping other stemmers here in case they are needed later
from stemming.porter2 import stem as p2stem
def stemWordP2(word):
    # bool for debugging, don't leave off
    stem = 1
    if stem:
        word = p2stem(word)
    return word

from nltk.stem import LancasterStemmer
def stemWordLancaster(word):
    # bool for debugging, don't leave off
    stem = 1
    if stem:
        #lancaster is more robust for group words than porter, but both NLTK modules have issues
        stemmer=LancasterStemmer()
        word = stemmer.stem(word)
    return word

from stemming.lovins import stem as lovins
def stemWordLovins(word):
    # bool for debugging, don't leave off
    stem = 1
    if stem:
        word = lovins(word)
    return word

from stemming.paicehusk import stem as paicehusk
def stemWordPaice(word):
    # bool for debugging, don't leave off
    stem = 1
    if stem:
        vowels = [p for p in word if p in "aeiou"]
        if len(vowels)>0:
            word = paicehusk(word)
    return word
